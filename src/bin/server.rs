use std::{
	net::{TcpListener, TcpStream},
	io::{Write, Read},
	collections::HashMap
};

macro_rules! unwrap_or_return {
	( $e:expr ) => {
		match $e {
			Ok(x) => x,
			Err(_) => return,
		}
	}
}

fn reply(mut stream: TcpStream, map: &mut HashMap<String, Vec<String>>) {
	let mut buffer = [0; 64000];
	let data = stream.read(&mut buffer[0..64000]).unwrap();
	let input = String::from_utf8((&buffer[0..data]).to_vec());
	match input {
		Ok(input) => {
			let lines_count = input.lines().count();
			if lines_count > 2 {
				let room = input.lines().last().unwrap().to_string();
				let line = input.lines().next().unwrap();
				match line {
				"update" => {
					if !map.contains_key(&room) {
						return;
					}
					let id = unwrap_or_return!(input.lines().nth(1).unwrap().parse::<usize>());
					let vec = map.get(&room).unwrap();
					if vec.len() > id {
						let r = [vec.len().to_string(), vec[id..].join("")].join("\n");
						stream.write_all(r.as_bytes()).unwrap();
					}
				}
				"input" => {
					if !map.contains_key(&room) {
						map.insert(room.clone(), vec![]);
					}
					let lines = input.lines().collect::<Vec<&str>>();
					let string = lines[1..(lines.len() - 1)].join("\n");
					let vec = map.get_mut(&room).unwrap();
					vec.push(string.clone());
					let r = [&vec.len().to_string(), "\n", &string].join("");
					stream.write_all(r.as_bytes()).unwrap();
				} _ => (), }
			} else {
				stream.write_all(&[0]).unwrap();
			}
		}
		Err(err) => println!("{:?}", err),
	}
}

fn main() -> () {
	let port = "14114";
	let host = "0.0.0.0";
	let addr = host.to_owned() + ":" + port;
	let listener = TcpListener::bind(addr).unwrap();
	let mut map: HashMap<String, Vec<String>> = HashMap::new();
	for stream in listener.incoming() {
		match stream {
			Ok(stream) => {
				reply(stream, &mut map);
			}
			Err(err) => println!("{:?}", err),
		}
	}
}
