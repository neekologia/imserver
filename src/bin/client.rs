use std::{
	io::{stdin, stdout, Stdout, Write, Read},
	net::{TcpStream, ToSocketAddrs, SocketAddr},
	time::Duration, thread, env
};
use termion::{
	clear, cursor,
	raw::{IntoRawMode, RawTerminal}
};

const IL: u16 = 10;
static mut ID: usize = 0;
static mut BUF: String = String::new();

fn spawn_read_updates(addr: SocketAddr, room: String) {
	let key = ["update", "\n", unsafe { &ID.to_string() }, "\n", &room].join("");
	let mut rt = stdout().into_raw_mode().unwrap();
	rt.suspend_raw_mode().unwrap();
	
	let mut stream = TcpStream::connect(addr).unwrap();
	stream.write(&key.as_bytes()).unwrap();
	read_reply(&stream, &mut rt);
	clear_input(&mut rt);

	thread::spawn(
		move || loop {
			let key = ["update", "\n", unsafe { &ID.to_string() }, "\n", &room].join("");
			let mut stream = TcpStream::connect(addr).unwrap();
			stream.write(&key.as_bytes()).unwrap();
			print!("{}", cursor::Save);
			read_reply(&stream, &mut rt);
			thread::sleep(Duration::from_millis(1000));
		}
	);
}

fn clear_input(rt: &mut RawTerminal<Stdout>) {
	write!(rt, "{}{}{}{}> {}",
		cursor::Goto(1, IL-1),
		clear::CurrentLine,
		cursor::Goto(1, IL),
		clear::CurrentLine,
		cursor::Save
	).unwrap();
	
	rt.flush().unwrap();
}

fn update_screen(string: String, rt: &mut RawTerminal<Stdout>) {
	let mut hterm: u16 = 2;
	for line in string.lines() {
		write!(rt, "{}{}{}",
			cursor::Goto(1, hterm-1),
			clear::CurrentLine,
			line).unwrap();
		if hterm > 8 {
			write!(rt, "{}{}{}{}>",
				termion::scroll::Up(1),
				cursor::Goto(1, IL-1),
				clear::CurrentLine,
				cursor::Goto(1, IL)
			).unwrap();
		} else {
			hterm += 1;
		}
	}
	
	rt.suspend_raw_mode().unwrap();
	print!("{}", cursor::Restore);
	rt.flush().unwrap();
}

fn read_reply(mut stream: &TcpStream, rt: &mut RawTerminal<Stdout>) {
	let mut string = String::new();
	let mut buffer = [0; 64000];
	let data = stream.read(&mut buffer[0..64000]);
	match data {
		Ok(data) => {
			let input = String::from_utf8_lossy(&buffer[0..data]).to_string();
			if input.lines().count() > 1 {
				let mut lines = input.lines();
				match lines.next().unwrap().parse::<usize>() {
					Ok(id) => {
						unsafe {
							ID = id;
							BUF += &(lines.collect::<Vec<&str>>().join("\n") + &'\n'.to_string());
							string = BUF.clone();
						}
// 						string = lines.collect();
					}
					Err(_) => (),
				}
			}
		}
		Err(err) => {
			println!("error: {:?}", err);
		}
	}
	update_screen(string, rt);
}

fn read_input(addr: SocketAddr, room: String) {
	let stdin = stdin();
	let mut rt = stdout().into_raw_mode().unwrap();
	rt.suspend_raw_mode().unwrap();
	let mut key = String::new();
	loop {
		match stdin.read_line(&mut key) {
			Ok(_n) => {
				if key == String::from("\n") { continue; }
				let mut stream = TcpStream::connect(addr).unwrap();
				let reply = {
					[ "input", &key, &room ].join("\n")
				};
// 				println!("reply: {}", reply);
				stream.write_all(reply.as_bytes()).unwrap();
				
				read_reply(&stream, &mut rt);
				clear_input(&mut rt);
				key = String::new();
			}
			Err(_) => (),
		}
	}
}

fn main() {
	let room = env::args().nth(1);
	match room {
		Some(room) => {
			print!("{}{}", clear::All, cursor::BlinkingBar);
			let domain = "archneek.zapto.org:14114";
			let addr = domain.to_socket_addrs().unwrap().next().unwrap();
			spawn_read_updates(addr, room.clone());
			read_input(addr, room);
		}
		None => {
			println!("room not specified");
		}
	}
}
